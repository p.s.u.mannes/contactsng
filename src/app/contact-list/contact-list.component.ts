import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../services/contacts.service';

//.ts means typescript file

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css'],
}) //@Decorator
export class ContactListComponent implements OnInit {
  constructor(private readonly contactService: ContactsService) {}

  ngOnInit(): void {
    this.contactService.fetchContacts;
  }
}
