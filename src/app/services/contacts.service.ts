import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';

@Injectable({
  providedIn: 'root',
})
export class ContactsService {
  //declaring contacts as an array of Contact
  private contacts: Contact[] = [];
  private error: string = '';

  //Dependency injection
  constructor(private readonly http: HttpClient) {}

  public fetchContacts(): void {
    //Angular does not use promises, it uses observables
    //.subscribe instead of .then
    //Angular framework parses to JSON automatically
    this.http.get<Contact[]>('http://localhost:3000/contacts').subscribe(
      (contacts: Contact[]) => {
        this.contacts = contacts;
      },
      (error: HttpErrorResponse) => {
        this.error = error.message;
      }
    );
  }
}
